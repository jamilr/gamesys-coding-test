package roulette.core;

import roulette.model.Bet;
import roulette.model.BetResult;
import roulette.model.BetType;
import roulette.model.Outcome;

import java.math.BigDecimal;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * BetResultProvider class implements the logic for calculating the player's outcome given the roulette iteration result and a player's bet
 *
 * The following rules are applied to calcualte the outcome
 *
 * if the bet is EVEN, and the roulette iteration result value is even, then the total win is the bet amount multiplied by 2
 * if the bet is ODD, and the roulette iteration result value is odd, then the total win is the bet amount multiplied by 2
 * if the bet is a number, and the roulette iteration result value is the same number, then the total win is the bet amount multiplied by 36
 *
 * If neither of the cases are true, then the player does not win the game
 */
public class BetResultProvider {

    public enum WinResultFactor {

        NUM(36),
        ODD(2),
        EVEN(2),
        NO_WIN(0);

        private int factor;

        WinResultFactor(int factor) {
            this.factor = factor;
        }

        public int getFactor() {
            return this.factor;
        }
    }

    /**
     * calc() -- calculates the total win a player gets given his bet and the roulette iteration result
     *
     * @param bet -- the bet for a given player
     *
     * @param result -- the result of the roulette iteration
     *
     *
     */
    public BetResult calc(Bet bet, int result) {

        BetType type = bet.getBetType();

        Outcome outcome = Outcome.LOSE;

        WinResultFactor winResultFactor = WinResultFactor.NO_WIN;;

        switch (type) {
            case ODD: {

                if ((result & 1) == 1) {
                    outcome = Outcome.WIN;
                    winResultFactor = WinResultFactor.ODD;
                }
            }
            break;
            case EVEN: {

                if ((result & 1) == 0) {
                    outcome = Outcome.WIN;
                    winResultFactor = WinResultFactor.EVEN;
                }
            }
            break;
            case NUMBER: {

                if (result == bet.getNumber()) {
                    outcome = Outcome.WIN;
                    winResultFactor = WinResultFactor.NUM;
                }
            }
            break;
            default:
        }

        BigDecimal winScore = bet.getAmount().multiply( new BigDecimal(winResultFactor.getFactor()));

        return new BetResult(outcome, winScore);
    }
}
