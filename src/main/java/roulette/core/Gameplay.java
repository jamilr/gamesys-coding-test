package roulette.core;

import roulette.model.BetResultHolder;
import roulette.model.Player;

import java.util.Map;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * Gameplay class creates the PlayerManager and Roulette threads and initiates the game
 *
 * BetResultHolder -- the shared in-memory object that holds the bet result emitted by Roulette and used later by PlayerManager
 *
 */
public class Gameplay {

    private Map<String, Player> playerMap;

    public Gameplay(Map<String, Player> players) {

        this.playerMap = players;
    }

    /**
     * start() -- starts the game by creating PlayerManager and Roulette
     *
     *
     */
    public void start() {

        System.out.println("Welcome to Roulette Game!");

        BetResultHolder betResultHolder = new BetResultHolder();

        Thread playerManager = new Thread(new PlayerManager(playerMap, betResultHolder));

        Thread roulette = new Thread( new Roulette(betResultHolder));

        playerManager.start();

        roulette.start();
    }
}
