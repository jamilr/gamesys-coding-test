package roulette.entry;

import roulette.util.ErrorMessages;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * GameOptionsParser -- helper class that checks whether there is a command line argument provided for starting a game
 *
 */
public class GameOptionsParser {

    /**
     * parse() -- a helper method that checks whether there is a command line argument provided for starting a game
     *
     * */
    public static String parse(String consoleOptions[]) {

        if (consoleOptions.length == 0) {

            System.out.println(ErrorMessages.NO_FILE_IS_PROVIDED);
            System.exit(-1);
        }

        return consoleOptions[0];
    }

}
