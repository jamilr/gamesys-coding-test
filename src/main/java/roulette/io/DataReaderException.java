package roulette.io;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * DataReaderException -- an exception to be used when InputDataReader encounters an invalid options for the player in the file
 *
 */
public class DataReaderException extends Exception {

    public DataReaderException(String msg) {
        super(msg);
    }
}
