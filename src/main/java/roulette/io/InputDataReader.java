package roulette.io;

import roulette.model.Player;
import roulette.model.util.PlayerParser;

import java.io.*;
import java.util.HashMap;
import java.util.Map;



/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * InputDataReader -- reads the players' input data from the given file
 *
 */
public class InputDataReader {

    /**
     *
     * @param filePath  - file path of the file with the player names
     *
     * readPlayers() -- reads players' options given the file path
     *
     * */
    public static Map<String, Player> readPlayers(String filePath) throws IOException, DataReaderException {

        Map<String, Player> playerMap = new HashMap<>();

        try (BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(new FileInputStream( new File(filePath))))) {

            String playerOptions;

            while ((playerOptions = bufferedReader.readLine()) != null) {

                Player player = PlayerParser.parse(playerOptions);

                playerMap.put(player.getName(), player);
            }
        }

        return playerMap;
    }
}