package roulette.io;

import roulette.model.BetType;
import roulette.model.Player;

import java.util.Map;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * ResultPrinter -- prints the roulette iteration results for all the players in the game
 *
 */
public class ResultPrinter {

    private static final String PLAYER_TOKEN = "Player: ";

    private static final String BET_TOKEN = "Bet: ";

    private static final String OUTCOME_TOKEN = "Outcome: ";

    private static final String WIN_TOKEN = "Winnings: ";

    private static final String TOTAL_WIN = "Total Win: ";

    private static final String TOTAL_BET = "Total Bet: ";

    private static final String DASH_ROW = "---";

    private static final String betOutcomeHeader = buildBetOutcomeHeader();

    private static final String totalOutcomeHeader = buildTotalOutcomeHeader();

    /**
     * buildBetOutcomeHeader() -- helper method for building a header for a single bet results
     *
     *
     * */
    private static String buildBetOutcomeHeader() {

        return new StringBuilder()
                .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.PLAYER_NAME,   PLAYER_TOKEN))
                .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,         BET_TOKEN))
                .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,         OUTCOME_TOKEN))
                .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,         WIN_TOKEN))
                .append(System.getProperty("line.separator"))
                .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE, DASH_ROW))
                .toString();
    }

    /**
     *
     * buildTotalOutcomeHeader() -- helper method for building a header for players' statistics (totalBet, totalWin)
     *
     * */
    private static String buildTotalOutcomeHeader() {

        return new StringBuilder()
                .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.PLAYER_NAME,   PLAYER_TOKEN))
                .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,         TOTAL_WIN))
                .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,         TOTAL_BET))
                .append(System.getProperty("line.separator"))
                .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE, DASH_ROW))
                .toString();
    }

    /**
     * print() -- prints the roulette iteration results for all the users as well as the players' statistics (totallWin, totalBet)
     *
     * */
    public void print(Map<String, Player> playerMap) {

        printCurBetResults(playerMap);
        printTotalResults(playerMap);
    }

    /**
     * printTotalResults() -- prints the roulette iteration results for all the players
     *
     * */
    public void printCurBetResults(Map<String, Player> playerMap) {

        System.out.println(betOutcomeHeader);

        StringBuilder playerResultsLineBuilder = new StringBuilder();

        for (String playerName : playerMap.keySet()) {

            Player player = playerMap.get(playerName);

            String betValue = (player.getCurBet().getBetType().equals(BetType.NUMBER))
                    ? Integer.toString(player.getCurBet().getNumber())
                    : player.getCurBet().getBetType().toString();

            System.out.println(playerResultsLineBuilder
                    .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.PLAYER_NAME,   playerName))
                    .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,   betValue))
                    .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,   player.getCurBetResult().getOutcome().toString()))
                    .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,   player.getCurBetResult().getWin().toString()))
                    .toString());

            playerResultsLineBuilder.setLength(0);
        }

        System.out.println();
    }

    /*
    *
    * printTotalResults() -- prints the players' statistics (totalWin, totalBet)
    *
    * */
    public void printTotalResults(Map<String, Player> playerMap) {

        System.out.println(totalOutcomeHeader);

        StringBuilder playerResultsLineBuilder = new StringBuilder();

        for (String playerName : playerMap.keySet()) {

            Player player = playerMap.get(playerName);

            System.out.println(playerResultsLineBuilder
                    .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.PLAYER_NAME,   playerName))
                    .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,   player.getTotalWin().toString()))
                    .append(ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE,   player.getTotalBet().toString()))
                    .toString());

            playerResultsLineBuilder.setLength(0);
        }

        System.out.println();
    }
}
