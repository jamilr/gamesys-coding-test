package roulette.io.console;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * ConsoleCommand - the enum that holds command line commands supported by a game
 *
 */
public enum ConsoleCommand {
    EXIT,
    NA;

    public static ConsoleCommand find(String value) {

        for (ConsoleCommand commmand : ConsoleCommand.values()){

            if (value.toLowerCase().equals(commmand.toString().toLowerCase()))
                return commmand;
        }

        return NA;
    }
}
