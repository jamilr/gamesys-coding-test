package roulette.io.console;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * ConsoleCommandException -- the exception that is thrown when invalid command is entered
 *
 */
public class ConsoleCommandException extends Exception {

    public ConsoleCommandException(String msg){
        super(msg);
    }
}
