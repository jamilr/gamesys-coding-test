package roulette.io.console;

import roulette.util.ErrorMessages;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 *  ConsoleCommandExecutor -- the helper class that executes the command entered by a user from the command line
 *
 *  EXIT - the only supported command that leads to application termination
 *
 */
public class ConsoleCommandExecutor {

    public static void execute(ConsoleCommand command) throws ConsoleCommandException {

        switch (command) {
            case EXIT: {

                System.out.println("Thanks you for playing the Roulette Game!");

                System.exit(-1);
            }
            default:{

                throw new ConsoleCommandException(ErrorMessages.COMMAND_NOT_FOUND);
            }
        }
    }
}
