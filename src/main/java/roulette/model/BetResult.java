package roulette.model;

import java.math.BigDecimal;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * BetResult class holds the result of the game against a particular bet
 *
 * It consists of two properties
 *
 * - outcome (Outcome) - the outcome of the bet (WIN, LOSE)
 * - win (BigDecimal) - the total win a player gets for a bet
 *
 */
public class BetResult {

    private Outcome outcome;

    private BigDecimal win;

    public BetResult(Outcome outcome, BigDecimal win) {
        this.outcome = outcome;
        this.win =win;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public BigDecimal getWin() {
        return win;
    }
}
