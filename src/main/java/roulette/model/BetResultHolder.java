package roulette.model;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * BetResultHolder class is used for holding a result of the roulette iteration
 *
 * It consits of two properties
 *
 * - newBetSet (Boolean) -- it indicates whether there is a new bet for all active users
 * - result (Integer) -- used by Roulette class to put a result of the roulette iteration
 *
  */
public class BetResultHolder {

    private Boolean newBetSet;

    private Integer result;

    public BetResultHolder() {

        this.newBetSet = false;
        this.result = null;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Boolean getNewBetSet() {
        return newBetSet;
    }

    public void setNewBetSet(Boolean newBetSet) {
        this.newBetSet = newBetSet;
    }
}