package roulette.model;

import roulette.util.TokenParser;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * BetType -- represents the type of the bet (EVEN, ODD, NUMBER, NA)
 *
 * - EVEN -- all even numbers in the range (1 - 36)
 * - ODD -- all odd numbers in the range (1 - 36)
 * - NUMBER -- a single number in the range (1 - 36)
 * - NA -- not a valid bet -- used for BetParser to indicate that bet is invalid
 *
 */
public enum BetType {
    EVEN,
    ODD,
    NUMBER,
    NA;

    public static BetType find(String betType) {

        for (BetType type : values()) {

            if (type.toString().equals(betType))
                return type;
        }

        if (TokenParser.isInt(betType)) return BetType.NUMBER;

        return BetType.NA;
    }
}
