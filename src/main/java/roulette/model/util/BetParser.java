package roulette.model.util;

import roulette.io.console.ConsoleCommand;
import roulette.io.console.ConsoleCommandException;
import roulette.io.console.ConsoleCommandExecutor;
import roulette.model.Bet;
import roulette.model.BetType;
import roulette.util.Constants;
import roulette.util.ErrorMessages;
import roulette.util.TokenParser;

import java.math.BigDecimal;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * BetParser -- parses the bet input for a single player provided from a command line
 *
 */
public class BetParser {

    /**
     *
     * parse() -- implements the parsing logic for a bet entered for a particular player from the command line
     *
     * @param playerBet -- the String object (command line) that contains the bet details
     *
     * */
    public static BetParserResult parse(String playerBet) throws BetParserException, ConsoleCommandException {

        String[] betOptions = playerBet.split(Constants.SPACE);

        String player = betOptions[0];

        ConsoleCommand consoleCommand = ConsoleCommand.find(player);

        if (!consoleCommand.equals(ConsoleCommand.NA))
            ConsoleCommandExecutor.execute(consoleCommand);

        String betValue = betOptions[1];

        BetType type = BetType.find(betValue);

        Bet.BetBuilder betBuilder = Bet.BetBuilder.getInstnace();

        switch (type) {

            case NUMBER: {

                int betNumber = Integer.parseInt(betValue);

                betBuilder.setNumber(betNumber);
            }
            break;
            case NA: {

                throw new BetParserException(ErrorMessages.BET_TYPE_INVALID);
            }
        }

        if (!TokenParser.isFloat(betOptions[2]))  new BetParserException(ErrorMessages.BET_AMOUNT_INVALID);

        betBuilder.setType(type);

        betBuilder.setAmount( new BigDecimal(betOptions[2]));

        Bet bet = betBuilder.build();

        return new BetParserResult(player, bet);
    }
}
