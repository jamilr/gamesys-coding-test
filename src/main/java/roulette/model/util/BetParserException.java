package roulette.model.util;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * BetParserException -- the exception thrown in case BetParser identifies the error in the player's bet
 *
 */
public class BetParserException extends Exception {

    public BetParserException(String value) {
        super(value);
    }
}
