package roulette.model.util;

import roulette.model.Bet;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * BetParserResult -- result of the bet parsing by BetParser
 *
 *
 */
public class BetParserResult {

    private String player;

    private Bet bet;

    public BetParserResult(String player, Bet bet) {
        this.player = player;
        this.bet = bet;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public Bet getBet() {
        return bet;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }
}
