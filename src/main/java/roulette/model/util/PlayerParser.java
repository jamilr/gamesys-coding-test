package roulette.model.util;

import roulette.io.DataReaderException;
import roulette.model.Player;
import roulette.util.Constants;
import roulette.util.ErrorMessages;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * PlayerParser -- implements the logic for parsing player's options
 *
 */
public class PlayerParser {

    /**
     *
     * parse() -- parses the player's options line
     *
     * @param optionsLine -- the String line with player's details
     *
     */
    public static Player parse(String optionsLine) throws DataReaderException {

        String[] options = optionsLine.split(Constants.COMMA);

        int optionsCount = options.length;

        String name = options[0];

        double totalWin = 0.0;
        double totalBet = 0.0;

        switch (optionsCount) {
            case 1:
            break;
            case 2: {
                totalWin = Double.parseDouble(options[1]);
            }
            break;
            case 3: {

                totalWin = Double.parseDouble(options[1]);
                totalBet = Double.parseDouble(options[2]);
            }
            break;
            default:{

                System.out.println(ErrorMessages.PLAYER_OPTIONS_NOT_VALID);

                throw new DataReaderException(ErrorMessages.PLAYER_DATA_NOT_VALID);
            }
        }

        return new Player(name, totalWin, totalBet);
    }
}
