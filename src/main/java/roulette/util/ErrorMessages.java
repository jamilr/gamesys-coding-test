package roulette.util;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * ErrorMessages -- the error messages used when an exception is thrown
 *
 */
public class ErrorMessages {

    public static final String NO_FILE_IS_PROVIDED = "Please, provide a valid path for the file with players' names";

    public static final String FAILURE_ON_READING_PLAYERS_FILE = "An error occurred while reading a file with players' names";

    public static final String PLAYER_OPTIONS_NOT_VALID = "Provided player options are not valid";

    public static final String BET_TYPE_INVALID = "Please, provide a valid bet for a player - EVEN, ODD, number (1 - 36)";

    public static final String BET_AMOUNT_INVALID = "Please, provide a valid bet amount";

    public static final String COMMAND_NOT_FOUND = "Please, provide a valid command";

    public static final String PLAYER_DATA_NOT_VALID = "Please, provide a file with a valid players' options";
}
