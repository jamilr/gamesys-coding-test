package roulette.util;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * GameMessages -- the messages used by the application
 *
 */
public class GameMessages {

    public static final String BET_MESSAGE = "Please, enter players' bets or type (exit) to exit a game";

    public static final String PLEASE_TRY_AGAIN_TO_ENTER_BET = "Please, try again to enter bet options for a player";
    public static final String PROVIDED_PLAYER_DOES_NOT_EXIST = "Please, provide a valid player name";
    public static final String PLAYER_GOT_NEW_BET = "A new bet will override the previous one for this player";
    public static final String BET_INPUT_ACKNOWLEDGEMENT = "Thank you for providing bets for all the players";
}
