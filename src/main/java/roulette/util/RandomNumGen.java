package roulette.util;

import java.util.Random;


/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 * RandomNumGen implements the random number generator for a range of value (1 - 36)
 *
 */
public class RandomNumGen {

    private static final int MIN = 0;
    private static final int MAX = 36;

    private Random random;

    public RandomNumGen() {

        random = new Random();
    }

    /**
     *
     * getNew() -- generates a random integer number in the range from MIN to MAX inclusively
     *
     * */
    public int getNew() {
        return MIN + random.nextInt((MAX - MIN) + 1);
    }
}
