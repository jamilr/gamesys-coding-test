package roulette.core;

import org.junit.Test;
import roulette.model.Bet;
import roulette.model.BetResult;
import roulette.model.BetType;
import roulette.model.Outcome;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 *
 */
public class BetResultProviderTest {

    @Test
    public void bet_odd_result_odd_return_win() {

        Bet bet = Bet.BetBuilder.getInstnace().setAmount( new BigDecimal(10)).setType(BetType.ODD).build();

        int result = 3;

        BetResultProvider betResultProvider = new BetResultProvider();

        BetResult betResult = betResultProvider.calc(bet, result);

        assertThat(betResult.getWin(), is( new BigDecimal(20.0)));
        assertThat(betResult.getOutcome(), is(Outcome.WIN));
    }

    @Test
    public void bet_even_result_even_return_win() {

        Bet bet = Bet.BetBuilder.getInstnace().setAmount( new BigDecimal(10)).setType(BetType.EVEN).build();

        int result = 4;

        BetResultProvider betResultProvider = new BetResultProvider();

        BetResult betResult = betResultProvider.calc(bet, result);

        assertThat(betResult.getWin(), is( new BigDecimal(20.0)));
        assertThat(betResult.getOutcome(), is(Outcome.WIN));
    }

    @Test
    public void bet_odd_result_even_return_loss() {

        Bet bet = Bet.BetBuilder.getInstnace().setAmount( new BigDecimal(10)).setType(BetType.ODD).build();

        int result = 4;

        BetResultProvider betResultProvider = new BetResultProvider();

        BetResult betResult = betResultProvider.calc(bet, result);

        assertThat(betResult.getWin(), is( new BigDecimal(0.0)));
        assertThat(betResult.getOutcome(), is(Outcome.LOSE));
    }

    @Test
    public void bet_even_result_odd_return_loss() {

        Bet bet = Bet.BetBuilder.getInstnace().setAmount( new BigDecimal(10)).setType(BetType.EVEN).build();

        int result = 3;

        BetResultProvider betResultProvider = new BetResultProvider();

        BetResult betResult = betResultProvider.calc(bet, result);

        assertThat(betResult.getWin(), is( new BigDecimal(0.0)));
        assertThat(betResult.getOutcome(), is(Outcome.LOSE));
    }

    @Test
    public void bet_number_result_matches_return_win() {

        Bet bet = Bet.BetBuilder.getInstnace().setAmount( new BigDecimal(10)).setType(BetType.NUMBER).setNumber(10).build();

        int result = 10;

        BetResultProvider betResultProvider = new BetResultProvider();

        BetResult betResult = betResultProvider.calc(bet, result);

        assertThat(betResult.getWin(), is( new BigDecimal(360.0)));
        assertThat(betResult.getOutcome(), is(Outcome.WIN));
    }

    @Test
    public void bet_number_result_does_not_match_return_loss() {

        Bet bet = Bet.BetBuilder.getInstnace().setAmount( new BigDecimal(10)).setType(BetType.NUMBER).setNumber(30).build();

        int result = 20;

        BetResultProvider betResultProvider = new BetResultProvider();

        BetResult betResult = betResultProvider.calc(bet, result);

        assertThat(betResult.getWin(), is( new BigDecimal(0.0)));
        assertThat(betResult.getOutcome(), is(Outcome.LOSE));
    }



}
