package roulette.io;

import org.junit.Test;
import roulette.model.Player;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 *
 */
public class InputDataReaderTest {

    @Test
    public void read_names() throws IOException,DataReaderException  {

        Set<String> expectedNamesSet = new HashSet<>();

        expectedNamesSet.add("James");
        expectedNamesSet.add("Sarah");

        String filePath = "test-data/names.txt";

        Map<String, Player> playerMap = InputDataReader.readPlayers(filePath);

        Set<String> names = playerMap.keySet();

        assertThat(names, equalTo(expectedNamesSet));

        assertThat(playerMap.get("James").getTotalBet().doubleValue(), is(0.0));
        assertThat(playerMap.get("James").getTotalWin().doubleValue(), is(0.0));
        assertThat(playerMap.get("Sarah").getTotalBet().doubleValue(), is(0.0));
        assertThat(playerMap.get("Sarah").getTotalWin().doubleValue(), is(0.0));
    }

    @Test
    public void read_names_and_total_bet_win_values() throws IOException,DataReaderException  {

        Set<String> expectedNamesSet = new HashSet<>();

        expectedNamesSet.add("James");
        expectedNamesSet.add("Sarah");

        String filePath = "test-data/names2.txt";

        Map<String, Player> playerMap = InputDataReader.readPlayers(filePath);

        Set<String> names = playerMap.keySet();

        assertThat(names, equalTo(expectedNamesSet));

        assertThat(playerMap.get("James").getTotalWin().doubleValue(), is(1.0));
        assertThat(playerMap.get("James").getTotalBet().doubleValue(), is(2.0));
        assertThat(playerMap.get("Sarah").getTotalWin().doubleValue(), is(2.0));
        assertThat(playerMap.get("Sarah").getTotalBet().doubleValue(), is(1.0));
    }

    @Test
    public void read_names_with_no_total_win_or_bet_provided() throws IOException, DataReaderException {

        Set<String> expectedNamesSet = new HashSet<>();

        expectedNamesSet.add("James");
        expectedNamesSet.add("Sarah");

        String filePath = "test-data/names3.txt";

        Map<String, Player> playerMap = InputDataReader.readPlayers(filePath);

        Set<String> names = playerMap.keySet();

        assertThat(names, equalTo(expectedNamesSet));

        assertThat(playerMap.get("James").getTotalWin().doubleValue(), is(1.0));
        assertThat(playerMap.get("James").getTotalBet().doubleValue(), is(0.0));
        assertThat(playerMap.get("Sarah").getTotalWin().doubleValue(), is(0.0));
        assertThat(playerMap.get("Sarah").getTotalBet().doubleValue(), is(0.0));
    }


    @Test(expected = DataReaderException.class)
    public void throw_exception_if_file_contains_invalid_data() throws IOException, DataReaderException {

        String filePath = "test-data/names4.txt";

        InputDataReader.readPlayers(filePath);
    }


}
