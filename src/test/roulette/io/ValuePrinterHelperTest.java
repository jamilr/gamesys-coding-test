package roulette.io;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 *
 */
public class ValuePrinterHelperTest {

    @Test
    public void print_player_name() {

        String playerName = "James";

        playerName = ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.PLAYER_NAME, playerName);

        String expected = "James               ";

        assertThat(expected, is(playerName));
    }

    @Test
    public void print_value() {

        String value = "EVEN";

        value = ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE, value);

        String expected = "EVEN           ";

        assertThat(expected, is(value));
    }

    @Test
    public void when_value_is_null() {

        String value = null;

        value = ValuePrinterHelper.insertValue(ValuePrinterHelper.ValueType.VALUE, value);

        String expected = "               ";

        assertThat(expected, is(value));
    }
}
