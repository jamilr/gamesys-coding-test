package roulette.model.util;

import org.junit.Test;
import roulette.io.console.ConsoleCommandException;
import roulette.model.Bet;
import roulette.model.BetType;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 *
 */
public class BetParserTest {

    @Test
    public void parse_odd_bet() throws BetParserException, ConsoleCommandException {

        String odd_bet = "James ODD 1.0";

        BetParserResult betParserResult = BetParser.parse(odd_bet);

        assertThat(betParserResult.getPlayer(), is("James"));

        Bet bet = betParserResult.getBet();

        assertThat(bet.getAmount().doubleValue(),   is(1.0));
        assertThat(bet.getBetType(),                is(BetType.ODD));
    }

    @Test
    public void parse_even_bet() throws BetParserException, ConsoleCommandException {

        String odd_bet = "James EVEN 1.0";

        BetParserResult betParserResult = BetParser.parse(odd_bet);

        assertThat(betParserResult.getPlayer(), is("James"));

        Bet bet = betParserResult.getBet();

        assertThat(bet.getAmount().doubleValue(),   is(1.0));
        assertThat(bet.getBetType(),                is(BetType.EVEN));
    }

    @Test
    public void parse_number_bet() throws BetParserException, ConsoleCommandException {

        String odd_bet = "James 20 1.0";

        BetParserResult betParserResult = BetParser.parse(odd_bet);

        assertThat(betParserResult.getPlayer(), is("James"));

        Bet bet = betParserResult.getBet();

        assertThat(bet.getAmount().doubleValue(),   is(1.0));
        assertThat(bet.getBetType(),                is(BetType.NUMBER));
        assertThat(bet.getNumber(),                 is(bet.getNumber()));
    }


    @Test(expected = BetParserException.class)
    public void parse_invalid_bet() throws BetParserException, ConsoleCommandException {

        String odd_bet = "James ABC 1.0";

        BetParser.parse(odd_bet);
    }


}
