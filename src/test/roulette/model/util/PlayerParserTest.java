package roulette.model.util;

import org.junit.Test;
import roulette.io.DataReaderException;
import roulette.model.Player;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Jamil Rzayev
 * Date : 27.03.2016
 *
 *
 */
public class PlayerParserTest {

    @Test
    public void parse_with_no_options() throws DataReaderException {

        String playerOptions = "James";

        Player player = PlayerParser.parse(playerOptions);

        assertThat(player.getName(), is("James"));
        assertThat(player.getTotalBet().doubleValue(),  is(0.0));
        assertThat(player.getTotalWin().doubleValue(),  is(0.0));
    }

    @Test
    public void parse_with_options() throws DataReaderException {

        String playerOptions = "James,1.0,2.0";

        Player player = PlayerParser.parse(playerOptions);

        assertThat(player.getName(), is("James"));
        assertThat(player.getTotalBet().doubleValue(),  is(2.0));
        assertThat(player.getTotalWin().doubleValue(),  is(1.0));
    }

    @Test(expected = DataReaderException.class)
    public void prse_with_invalid_options() throws DataReaderException {

        String playerOptions = "James,1.0,2.0,4.0";

        PlayerParser.parse(playerOptions);
    }
}
